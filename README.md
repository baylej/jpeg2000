# JPEG2000

jpeg2000 is a project to implement the jpeg2000 standard

## Licence ##

Copyright (C) 2015  Bardoux, Bayle

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see [http://www.gnu.org/licenses/](http://www.gnu.org/licenses/).

## Building

```bash
git clone git@bitbucket.org:baylej/jpeg2000.git
cd jpeg2000
make
```