CC= gcc
CFLAGS= -Wall -g -c
LFLAGS= -g -lm
SRCDIR= src
SOURCES= $(wildcard $(SRCDIR)/*.c)
OBJECTS= $(SOURCES:.c=.o)
TARGET= jpeg2000

all: $(TARGET)

$(TARGET): $(OBJECTS)
	$(CC) -o $@ $^ $(LFLAGS)

%.o: %.c
	$(CC) -o $@ $(CFLAGS) $^

.PHONY: clean mrproper

clean:
	rm $(SRCDIR)/*.o

mrproper: clean
	rm $(TARGET)

