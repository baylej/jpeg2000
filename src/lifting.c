#include <stdlib.h>
#include <stdio.h>
#include <string.h>

void prediction_n(double *x, int p, double a){
    int i, index1, index2;
    for(i = 1; i<p; i+=2){
        index1 = (i-1 < 0) ? 1 : i-1; // effet miroir
        index2 = (i+1>=p) ? (p-2) : i+1; // effet miroir
        x[i] = a * x[index1] + x[i] + a*x[index2];
    }
}

void mise_a_jour_n(double *x, int p, double a){
    int i, index1, index2;
    for(i = 0; i<p; i+=2){
        index1 = (i-1 < 0) ? 1 : i-1; // effet miroir
        index2 = (i+1>=p) ? (p-2) : i+1; // effet miroir
        x[i] = a * x[index1] + x[i] + a*x[index2];
    }
}

void mise_a_echelle(double *x, int p, double a){
    int i;
    for(i=0; i<p; i++){
        if(i%2 == 0){ // pair
            x[i] = x[i]/a;
        }
        else { // impair
            x[i] = x[i]*a;
        }
    }
}

int analyse_97_lifting(double* x, int p){
    double a;
    int i;
    double *X;

    // prédiction 1
    a =-1.586134342;
    prediction_n(x, p, a);

    // mise à jour 1
    a=-0.05298011854;
    mise_a_jour_n(x, p, a);

    // prédiction 2
    a=0.8829110762;
    prediction_n(x, p, a);

    // mise à jour 2
    a=0.4435068522;
    mise_a_jour_n(x, p, a);

    // mise à l'échelle
    a=1/1.149604398;
    for(i=0; i<p; i++){
        if(i%2 == 0){ // pair
            x[i] = x[i]/a;
        }
        else { // impair
            x[i] = x[i]*a;
        }
    }

    // mise en forme
    X = malloc(p * sizeof(double));
    if(X == NULL) return -1;
    memcpy(X, x, p*sizeof(double));

    for(i = 0; i<p; i++){
        if(i%2 == 0) // pair
            x[i/2] = X[i];
        else // impair
            x[p/2+i/2] = X[i];
    }
    free(X);

    return 0;
}

int synthese_97_lifting(double* x,int p){
    double *X;
    double a;
    int i;

    // mise en forme
    X = malloc(p * sizeof(double));
    if(X == NULL) return -1;
    memcpy(X, x, p*sizeof(double));

    for(i = 0; i<p; i++){
        if(i%2 == 0) // pair
            x[i] = X[i/2];
        else // impair
            x[i] = X[p/2+i/2];
    }
    free(X);

    // mise à l'échelle
    a=1.149604398;
    mise_a_echelle(x, p, a);

    // mise à jour 2
    a=-0.4435068522;
    mise_a_jour_n(x, p, a);

    // prédiction 2
    a=-0.8829110762;
    prediction_n(x, p, a);

    // mise à jour 1
    a=0.05298011854;
    mise_a_jour_n(x, p, a);

    // prédiction 1
    a=1.586134342;
    prediction_n(x, p, a);

    return 0;
}