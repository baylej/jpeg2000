#ifndef JPG2000_H
#define JPG2000_H
#pragma once

#include <inttypes.h>

//// Fichier haar.c

/// Interpolation de facteur 2
/// double la taille de `x` et insère la valeur `0.0` aux indices impairs
/// len EST LA TAILLE DU TABLEAU EN SORTIE (2 fois la taille de `x`).
int interpolation2(double **x, int len);

/// Decimation de facteur 2
int decimation2(double **x, int len);

/// Convolution 
int convolution(double **x, int p, double *h, int q);

/// Analyse de Haar
int analyse_haar(double** x,int p);

/// Reconstruction du signal avec Haar
int synthese_haar(double** x,int p);

/// Analyse par filtre biorthogonaux 9/7
int analyse_97(double** x,int p);

/// Reconstruction par filtre biorthogonaux 9/7
int synthese_97(double** x,int p);

/// Analyse et reconstruction par un filtre fourni
int analyse_filtre(double** x, int p, double *_h0, int lenH0, double *_h1, int lenH1);

int synthese_filtre(double** x, int p, double *_g0, int lenG0, double *_g1, int lenG1);




//// Fichier lifting.c

int analyse_97_lifting(double* x,int p);

int synthese_97_lifting(double* x,int p);


//// Fichier amr.c

void statAmr(double *x, int p, double *min, double *max, double *avg);

int amr(double** x, int p, int niveau);

int amrWithStat(double** x, int p, int niveau);

int iamr(double** x,int p,int niveau);


//// Fichier matrice2D

void statMat2D(double *x, int realLen, int realHei, int offLen, int offHei, int reqLen, int reqHei, double *avg, double *var, double *min, double *max);

double varMat2D(double *x, int realLen, int realHei, int offLen, int offHei, int reqLen, int reqHei);

void getColumn(double * x, int realLen, int reqHei, double *col, int k);

void setColumn(double *x, int realLen, int reqHei, double * col, int k);

void analyse2D_97(double *x, int realLen, int realHei, int reqLen, int reqHei);

void synthese2D_97(double *x, int realLen, int realHei, int reqLen, int reqHei);

void amr2D_97(double* m,int p,int j);

void amr2D_97WithStat(double* m, int p, int niveau, double *better_output);

void iamr2D_97(double* m,int p,int j);


//// Fichier io.c

/// Lit un signal d'un fichier ASCII
void read_signal(double* x,int n,char* filename);

/// Affiche un signal sur le terminal
void print_signal(double* x,int n);

/// Sauvegarde un signal dans le fichier filename
void save_signal(double* x,int n,char* filename);

/// Charge une image dans un tableau de double
double* charge_bmp256(char* fichier, uint32_t* largeur, uint32_t* hauteur);

/// Ecrit une image dans un fichier à partir d'un tableau.
int ecrit_bmp256(char* fichier, uint32_t largeur, uint32_t hauteur, double* m);


//// Fichier psnr.c

/// Erreur Quadratique Moyenne
double EQM(double *a, double *b, int len);

/// Peak Signal Noise Ratio
double PSNR(double *a, double *b, int len);


//// Fichier quantlm.c

void quantlm_idx(double* x,int n,int nq);

void quantlm(double* x,int n,int nq);

#endif /*JPG2000_H*/
