#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define UNRACDEUX 0.7071067811865475

int interpolation2(double **x, int len) {
	int i;
	
	double *y = calloc(len, sizeof(double));
	if (y==NULL) return -1;
	
	for (i=0; i<len/2; i++) {
		*(y+2*i) = *(*x+i);
	}
	
	free(*x);
	*x = y;
	return 0;
}

int decimation2(double **x, int len) {
	int i;
	
	double *y = calloc(len, sizeof(double));
	if (y==NULL) return -1;
	
	for (i=0; i<len/2; i++) {
		*(y+i) = *(*x+2*i);
	}
	
	free(*x);
	*x = y;
	return 0;
}

int convolution(double **x, int p, double *h, int q){
	double sum;
	int n, k, nn;
	
	double *y = calloc(p, sizeof(double));
	if(y==NULL) return -1;
	
	for(n=0; n<p; n++) {
		sum = 0;
		for(k=-q/2; k<=q/2; k++) {
			if(n-k >= p) nn = p-1 - ((n-k) - p+1);
			else if (n-k < 0) nn = -(n-k);
			else nn = n - k;
			sum += h[k+q/2] * (*x)[nn];

		}
		y[n] = sum;
	}
	free(*x);
	*x = y;
	return 0;
}

int analyse_filtre(double** x, int p, double *_h0, int lenH0, double *_h1, int lenH1){
	double *xx, *y;
	
	xx = malloc(p*sizeof(double));
	memcpy(xx, *x, p*sizeof(double));
	
	if(convolution(  x, p, _h0, lenH0)) return -1;
	if(convolution(&xx, p, _h1, lenH1)) return -1;
	
	if(decimation2(  x, p)) return -1;
	if(decimation2(&xx, p)) return -1;
	
	y = malloc(p*sizeof(double));
	if(y == NULL) return -1;
	
	memcpy(    y, *x, (p/2)*sizeof(double));
	memcpy(y+p/2, xx, (p/2)*sizeof(double));

	free(xx);
	free(*x);
	
	*x = y;
	
	return 0;
}

int synthese_filtre(double** x, int p, double *_g0, int lenG0, double *_g1, int lenG1){
	double *xx;
	int i;
	
	xx = malloc(p*sizeof(double));
	if(xx == NULL) return -1;
	memcpy(xx, (*x)+p/2, (p/2)*sizeof(double));
	
	if(interpolation2(  x, p)) return -1;
	if(interpolation2(&xx, p)) return -1;
	
	if(convolution(  x, p, _g0, lenG0)) return -1;
	if(convolution(&xx, p, _g1, lenG1)) return -1;
	
	for(i = 0; i < p; i++) {
		(*x)[i] += xx[i];
	}
	
	free(xx);
	
	return 0;
}

int analyse_haar(double** x, int p){
	double _h0[] = {UNRACDEUX,  UNRACDEUX, 0};
	double _h1[] = {UNRACDEUX, -UNRACDEUX, 0};
	
	return analyse_filtre(x, p, _h0, 3, _h1, 3);
}

int synthese_haar(double** x,int p){
	double _g0[] = {0,  UNRACDEUX, UNRACDEUX};
	double _g1[] = {0, -UNRACDEUX, UNRACDEUX};

	return synthese_filtre(x, p, _g0, 3, _g1, 3);
}

int analyse_97(double** x,int p){
	double _h0[9], _h1[9];

	// Filtre biorthogonal 9/7 _h0 (longueur 9)
	_h0[0]=0.037828455507;
	_h0[1]=-0.023849465019;
	_h0[2]=-0.110624404418;
	_h0[3]=0.377402855613;
	_h0[4]=0.852698679009;
	_h0[5]=0.377402855613;
	_h0[6]=-0.110624404418;
	_h0[7]=-0.023849465019;
	_h0[8]=0.037828455507;

	// Filtre biorthogonal 9/7 _h1 (longueur 9)
	_h1[0]=0.064538882629;
	_h1[1]=-0.040689417610;
	_h1[2]=-0.418092273222;
	_h1[3]=0.788485616406;
	_h1[4]=-0.418092273222;
	_h1[5]=-0.040689417610;
	_h1[6]=0.064538882629;
	_h1[7]=0.000000000000;
	_h1[8]=-0.000000000000;

	return analyse_filtre(x, p, _h0, 9, _h1, 9);
}

int synthese_97(double** x,int p){
	double _g0[7], _g1[11];
	// Filtre biorthogonal 9/7 _g0 (longueur 7)
	_g0[0]=-0.064538882629;
	_g0[1]=-0.040689417610;
	_g0[2]=0.418092273222;
	_g0[3]=0.788485616406;
	_g0[4]=0.418092273222;
	_g0[5]=-0.040689417610;
	_g0[6]=-0.064538882629;

	// Filtre biorthogonal 9/7 _g1 (longueur 11)
	_g1[0]=0.000000000000;
	_g1[1]=-0.000000000000;
	_g1[2]=0.037828455507;
	_g1[3]=0.023849465019;
	_g1[4]=-0.110624404418;
	_g1[5]=-0.377402855613;
	_g1[6]=0.852698679009;
	_g1[7]=-0.377402855613;
	_g1[8]=-0.110624404418;
	_g1[9]=0.023849465019;
	_g1[10]=0.037828455507;

	return synthese_filtre(x, p , _g0, 7, _g1, 11);
}
