#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "jpeg2000.h"

double test_error(double *x, double *y, double *error, int len){
	int i;
	double sum = 0;
	for(i = 0; i<len; i++){
		error[i] = (y[i] - x[i])*(y[i] - x[i]);
		sum += error[i];
	}
	return sum;
}

void test_amr(const char * fichier, int len, int niveau){
	double *x, *y, *error; 
	char filename[256], out[256], err[256], mid[256];
	sprintf(filename, "%s.txt", fichier);
	sprintf(out, "out_%s_amr_%d.txt", fichier, niveau);
	sprintf(err, "err_%s_amr_%d.txt", fichier, niveau);
	sprintf(mid, "mid_%s_amr_%d.txt", fichier, niveau);

	y = calloc(len, sizeof(double));
	read_signal(y, len, filename);

	x = malloc(len*sizeof(double));
	memcpy(x, y, len*sizeof(double));

	amrWithStat(&x, len, niveau);
	save_signal(x, len, mid);
	iamr(&x, len, niveau);


	save_signal(x, len, out);

	error = calloc(len, sizeof(double));
	test_error(x, y, error, len);
	save_signal(error, len, err);

	free(x);
	free(y);
	free(error);
}

void test_haar(const char * fichier, int len){
	double *x, *y, *error; 
	char filename[256], out[256], err[256], coef[256];
	sprintf(filename, "%s.txt", fichier);
	sprintf(out, "out_%s_haar.txt", fichier);
	sprintf(err, "err_%s_harr.txt", fichier);
	sprintf(coef,"coef_%s_haar.txt",fichier);

	// signal original
	y = calloc(len, sizeof(double));
	read_signal(y, len,filename);

	x = malloc(len*sizeof(double));
	memcpy(x, y, len*sizeof(double));
	analyse_haar(&x, len);
	save_signal(x, len, coef);
	synthese_haar(&x, len);
	save_signal(x, len, out);

	error = calloc(len, sizeof(double));
	test_error(x, y, error, len);
	save_signal(error, len, err);

	free(x);
	free(y);
	free(error);
}

void test_97(const char * fichier, int len){
	double *x, *y, *error; 
	char filename[256], out[256], err[256], coef[256];
	sprintf(filename, "%s.txt", fichier);
	sprintf(out, "out_%s_97.txt", fichier);
	sprintf(err, "err_%s_97.txt", fichier);
	sprintf(coef,"coef_%s_97.txt",fichier);

	// signal original
	y = calloc(len, sizeof(double));
	read_signal(y, len,filename);

	x = malloc(len*sizeof(double));
	memcpy(x, y, len*sizeof(double));
	analyse_97(&x, len);
	save_signal(x, len, coef);
	synthese_97(&x, len);
	save_signal(x, len, out);

	error = calloc(len, sizeof(double));
	test_error(x, y, error, len);
	save_signal(error, len, err);

	free(x);
	free(y);
	free(error);
}

void test_97_lifting(const char *fichier, int len){
	double *x, *y, *error; 
	char filename[256], out[256], err[256], coef[256];
	sprintf(filename, "%s.txt", fichier);
	sprintf(out, "out_%s_97_lifting.txt", fichier);
	sprintf(err, "err_%s_97_lifting.txt", fichier);
	sprintf(coef,"coef_%s_97_lifting.txt",fichier);

	y = calloc(len, sizeof(double));
	read_signal(y, len, filename);

	x = malloc(len*sizeof(double));
	memcpy(x, y, len*sizeof(double));
	analyse_97_lifting(x, len);
	save_signal(x, len, coef);
	synthese_97_lifting(x, len);
	save_signal(x, len, out);

	error = calloc(len, sizeof(double));
	test_error(x, y, error, len);
	save_signal(error, len, err);

	free(x);
	free(y);
	free(error);
}

void test_decomposition_2D(const char *fichier){
	double *x, *y, *error;
	uint l, h;
	char filename[256], out[256], err[256], mid[256];
	sprintf(filename, "%s.bmp", fichier);
	sprintf(out, "out_%s_decompostion_2d.bmp", fichier);
	sprintf(mid, "mid_%s_decompostion_2d.bmp", fichier);
	sprintf(err, "err_%s_decompostion_2d.bmp", fichier);

	y = charge_bmp256(filename, &l, &h);

	x = malloc(l * h * sizeof(double));
	memcpy(x, y, l*h*sizeof(double));

	analyse2D_97(x, l, h, l, h);
	ecrit_bmp256(mid, l, h, x);
	synthese2D_97(x, l, h, l, h);
	ecrit_bmp256(out, l, h, x);

	error = calloc(l*h, sizeof(double));
	test_error(x, y, error, l*h);
	ecrit_bmp256(err, l, h, error);

	free(x);
	free(y);
	free(error);
}

void test_decomposition_2D_amr(const char * fichier){
	double *x, *y, *sexy_out, *error;
	uint l, h;
	char filename[256], out[256], err[256], mid[256];
	sprintf(filename, "%s.bmp", fichier);
	sprintf(out, "out_%s_decompostion_2d_amr.bmp", fichier);
	sprintf(mid, "mid_%s_decompostion_2d_amr.bmp", fichier);
	sprintf(err, "err_%s_decompostion_2d_amr.bmp", fichier);

	y = charge_bmp256(filename, &l, &h);

	x = malloc(l * h * sizeof(double));
	memcpy(x, y, l*h*sizeof(double));
	
	sexy_out = malloc(l * h * sizeof(double));

	if(l==h){
		amr2D_97WithStat(x, l, 3, sexy_out);
		ecrit_bmp256(mid, l, h, sexy_out);

		iamr2D_97(x, l, 3);
		ecrit_bmp256(out, l, h, x);
	}

	error = calloc(l*h, sizeof(double));
	test_error(x, y, error, l*h);
	ecrit_bmp256(err, l, h, error);

	free(x);
	free(y);
	free(error);
}

int main(int argc, char *argv[]) {
	/*
	 * Test decomposition 2D AMR
	 */
	//*
	test_decomposition_2D_amr("lena");
	//*/

	/*
	 * Test Decomposition 2D
	 */
	/*
	test_decomposition_2D("lena");
	//*/

	/*
	 * Test AMR
	 */
	/*
	test_amr("test", 512, 2);
	test_amr("test", 512, 4);
	test_amr("test", 512, 9);
	//*/

	/*
	 * test du lifting
	 */
	/*
	test_97_lifting("leleccum", 4096);
	test_97("leleccum", 4096);
	test_haar("leleccum", 4096);

	test_97_lifting("test", 512);
	test_97("test", 512);
	test_haar("test", 512);
	*/

	/*
	 * Test du signal rampe par les filtres Haar et 9/7
	 */
	 /*
	double *rampe;
	int i;
	
	rampe = malloc(256*sizeof(double));
	if(rampe == NULL) return -1;
	
	for(i = 0; i<256; i++) {
		rampe[i] = i;
	}

	analyse_haar(&rampe, 256);
	save_signal(rampe, 256, "coef_rampe.txt");
	synthese_haar(&rampe, 256);
	
	save_signal(rampe, 256, "out_rampe.txt");
	
	for(i = 0; i<256; i++) {
		rampe[i] = i;
	}

	analyse_97(&rampe, 256);
	save_signal(rampe, 256, "coef_rampe_97.txt");
	synthese_97(&rampe, 256);
	
	save_signal(rampe, 256, "out_rampe_97.txt");

	free(rampe);
	*/

	/*
	 * Test de la fonction de convolution
	 */
	/*
	double *array, *h;
	int i, len = 5, q=3;
	
	array = malloc(len * sizeof(double));
	if (array == NULL) return -1;
	
	h=malloc(q * sizeof(double));
	if (h == NULL) return -1;
	
	for(i=0; i<len; i++) {
		array[i] = (double)i;
	}
	
	for(i=0; i<q; i++) {
		h[i] = (double)i+1;
	}
	
	convolution(&array, len, h, q);
	
	for(i=0; i<len; i++){
		printf("%lf\n", array[i]);
	}

	free(array);
	free(h);
	*/
	

	/*
	 * Test d'interpolation et decimation par 2
	**/
	/*
	double *array;
	int i, len = 10;
	array=malloc(len/2 * sizeof(double));
	if (array == NULL) return -1;
	
	for(i=0; i<len/2; i++) {
		array[i] = (double)i;
	}
	
	if (interpolation2(&array, len) != 0) {
		return -1;
	}
	
	for(i=0; i<len; i++) {
		printf("%lf\n", array[i]);
	}
	
	if (decimation2(&array, len) != 0) {
		return -1;
	}
	
	putchar('\n');
	for(i=0; i<len; i++) {
		printf("%lf\n", array[i]);
	}

	free(array)
	*/
	
	
	return 0;
}

