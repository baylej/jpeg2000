#include <stdio.h>
#include <stdlib.h>
#include <math.h>

//#include "jpeg2000.h"

// Erreur Quadratique Moyenne
double EQM(double *a, double *b, int len) {
	int p;
	double eqm = 0, v;
	
	for (p=0; p<len; p++) {
		v = *(a+p) - *(b+p);
		eqm += v * v;
	}
	
	eqm *= 1.d / (double)len;
	
	return eqm;
}

// Peak Signal Noise Ratio
double PSNR(double *a, double *b, int len) {
	double eqm = EQM(a, b, len),
	       psnr = 10.d * log10(65025/eqm);
	return psnr;
}

