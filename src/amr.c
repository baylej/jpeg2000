#include <stdio.h>
#include <math.h>

#include "jpeg2000.h"

void statAmr(double *x, int p, double *min, double *max, double *avg){
    int i;
    double sum = 0;
    *min = x[0];
    *max = x[0];
    for(i=1; i<p; i++){
        if(x[i] < *min)
            *min = x[i];
        if(x[i] > *max)
            *max = x[i];
        sum += x[i];
    }
    *avg = sum/(double)p;
}

int amr(double** x, int p, int niveau){
    int i;
    int arret;

    if(log2f((float) p) < niveau || niveau < 1)
        niveau = log2f((float) p);

    arret = pow(2, niveau);
    for(i=1; i<arret; i*=2){
        analyse_97_lifting(*x, p/i);
    }
    return 0;
}

int amrWithStat(double** x, int p, int niveau){
    int i;
    int arret;
    double min, max, avg;

    if(log2f((float) p) < niveau || niveau < 1)
        niveau = log2f((float) p);

    arret = pow(2, niveau);
    for(i=1; i<arret; i*=2){
        analyse_97_lifting(*x, p/i);
        statAmr(*x+(p/(i*2)),p/(i*2), &min, &max, &avg);
        printf("Intervalle : %d - %d\n\tmin : %lf\n\tmax : %lf\n\tmoyenne : %lf\n"
            ,p/(i*2), p/(i*2) + p/(i*2)
            ,min
            ,max
            ,avg);
    }
    
    statAmr(*x,p/i, &min, &max, &avg);
    printf("Intervalle : 0 - %d\n\tmin : %lf\n\tmax : %lf\n\tmoyenne : %lf\n\n"
            ,p/i
            ,min
            ,max
            ,avg);

    return 0;
}

int iamr(double** x,int p,int niveau){
    int i;
    int arret;

    if(log2f((float) p) < niveau || niveau < 1)
        niveau = log2f((float) p);

    arret = pow(2, niveau-1);
    for(i=arret; i>0; i/=2){
        synthese_97_lifting(*x, p/i);
    }

    return 0;
}
