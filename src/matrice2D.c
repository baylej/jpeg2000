#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "jpeg2000.h"

/*
 * x le signal 2D
 * realLen la largeur du signal 2D
 * realHei la hauteur du signal 2D
 * offLen le décallage en largeur (x0)
 * offHei le décallage en hauteur (y0)
 * reqLen la taille en largeur du carré étudié
 * reqHei la taille en hauteur du carré étudié
 * avg out moyenne
 * var out variance
 * min out minimum (peut etre NULL)
 * max out maximum (peut etre NULL)
 */
void statMat2D(double *x, int realLen, int realHei, int offLen, int offHei, int reqLen, int reqHei,
	           double *avg, double *var, double *min, double *max){
    int i, j;
    double sum = 0, sqsum = 0;

    if(offLen + reqLen > realLen)
        reqLen = realLen - offLen;
    if(offHei + reqHei > realHei)
        reqHei = realHei - offHei;
    
    if (min) *min = x[offHei*realLen+offLen];
    
    if (max) *max = x[offHei*realLen+offLen];
    
    for(j=offHei; j<offHei+reqHei; j++){
        for(i=offLen; i<offLen + reqLen; i++){
            sum += x[j*realLen+i];
            sqsum += x[j*realLen+i] * x[j*realLen+i];
            if (max && x[j*realLen+i] > *max)
            	*max = x[j*realLen+i];
            if (min && x[j*realLen+i] < *min)
            	*min = x[j*realLen+i];
        }
    }
    
    *avg =   sum/(double)(reqLen * reqHei);
    *var = sqsum/(double)(reqLen * reqHei) - (*avg) * (*avg);
}

/*
 * k is the column needed
 */
void getColumn(double * x, int realLen, int reqHei, double *col, int k){
    int i;

    for(i=0; i < reqHei; i++){
        col[i] = x[i*realLen + k];
    }
}

/*
 * k is the column to set
 */
void setColumn(double *x, int realLen, int reqHei, double * col, int k){
    int i;

    for(i=0; i < reqHei; i++){
        x[i*realLen+k] = col[i];
    }
}

void analyse2D_97(double *x, int realLen, int realHei, int reqLen, int reqHei){
    int i;
    double * col;

    col = (double *) malloc(reqHei * sizeof(double));
    if(NULL == col) exit(-1);
    
    for(i=0; i < reqHei; i++){
        analyse_97_lifting(x + (i*realLen), reqLen);
    }

    for(i=0; i<reqLen; i++){
        getColumn(x, realLen, reqHei, col, i);
        analyse_97_lifting(col, reqHei);
        setColumn(x, realLen, reqHei, col, i);
    }
    free(col);
}

void synthese2D_97(double *x, int realLen, int realHei, int reqLen, int reqHei){
    int i;
    double *col;

    col = (double *) malloc(reqHei * sizeof(double));
    if(NULL == col) exit(-1);

    for(i=0; i<reqLen; i++){
        getColumn(x, realLen, reqHei, col, i);
        synthese_97_lifting(col, reqHei);
        setColumn(x, realLen, reqHei, col, i);
    }
    free(col);

    for(i=0; i < reqHei; i++){
        synthese_97_lifting(x + (i*realLen), reqLen);
    }
}


void amr2D_97(double* m, int p, int niveau){
    int i;
    int arret;

    if(log2f((float) p) < niveau || niveau < 1)
        niveau = log2f((float) p);

    arret = pow(2, niveau);

    for(i=1; i<arret; i*=2){
        analyse2D_97(m, p, p, p/i, p/i);
    }
}

void betterCopy(double *src, double *dst,
	          int realLen, int realHei, int offLen, int offHei, int reqLen, int reqHei,
	          double min, double max, int bool_add127){
	int i, j;
	double ratio = (max - min) / 255.0d;
	
	if(offLen + reqLen > realLen)
		reqLen = realLen - offLen;
	if(offHei + reqHei > realHei)
		reqHei = realHei - offHei;
	
	for(j=offHei; j<offHei + reqHei; j++){
		for(i=offLen; i<offLen + reqLen; i++){
			dst[j*realLen+i] = src[j*realLen+i];
			if (bool_add127)
				dst[j*realLen+i] += 127;
			else
				dst[j*realLen+i] = (dst[j*realLen+i] - min) / ratio;
		}
	}
}

void amr2D_97WithStat(double* m, int p, int niveau, double *better_output){
    int j;
    double i;
    int arret;
    double min, max;
    double avgH, avgV, avgD, avgA;
    double varH, varV, varD, varA, varP = 1, b = 1;
    double *bi, *vars;

    if(log2f((float) p) < niveau || niveau < 1)
        niveau = log2f((float) p);

    arret = pow(2, niveau);
    bi   = (double *) malloc((niveau*3+1) * sizeof(double));
    vars = (double *) malloc((niveau*3+1) * sizeof(double));

    for(i=1; i<arret; i*=2){
        analyse2D_97(m, p, p, p/i, p/i);
    }
    
    for(i=1, j=0; i<arret; i*=2, j+=3){
    	double Nj_N;
    	
        /* coef detail H */
        printf("coefH : ");
        statMat2D(m, p, p, p/(i*2), 0, p/(i*2), p/(i*2), &avgH, &varH, &min, &max);
        printf("avgH varH niveau : %lf : %lf %lf\n", i, avgH, varH);
        if (better_output) betterCopy(m, better_output, p, p, p/(i*2), 0, p/(i*2), p/(i*2), min, max, 1);
        
        /* coef detail V */
        printf("coefV : ");
        statMat2D(m, p, p, 0, p/(i*2), p/(i*2), p/(i*2), &avgV, &varV, &min, &max);
        printf("avgV varV niveau : %lf : %lf %lf\n", i, avgV, varV);
        if (better_output) betterCopy(m, better_output, p, p, 0, p/(i*2), p/(i*2), p/(i*2), min, max, 1);
        
        /* coef detail D */
        printf("coefD : ");
        statMat2D(m, p, p, p/(i*2), p/(i*2), p/(i*2), p/(i*2), &avgD, &varD, &min, &max);
        printf("avgD varD niveau : %lf : %lf %lf\n", i, avgD, varD);
        if (better_output) betterCopy(m, better_output, p, p, p/(i*2), p/(i*2), p/(i*2), p/(i*2), min, max, 1);

        Nj_N = (p/(i*2))*(p/(i*2)) / (double)(p*p);
        
        varP *= pow(varH * varH, Nj_N)
             *  pow(varV * varV, Nj_N)
             *  pow(varD * varD, Nj_N);
        
        vars[j]   = varH;
        vars[j+1] = varV;
        vars[j+2] = varD;
    }

    statMat2D(m, p, p, 0, 0, p/i, p/i, &avgA, &varA, &min, &max);
    printf("avgA varA : %lf %lf\n", avgA, varA);
    betterCopy(m, better_output, p, p, 0, 0, p/i, p/i, min, max, 0);

    varP *= pow(varA * varA, ((p/i)*(p/i))/(double)(p*p));
    vars[j] = varA;
    
    for(j=0; j<niveau*3+1; j++){
        bi[j] = (b + 0.5d * log2((vars[j]*vars[j]) / varP));
        printf("bi : %lf\n",bi[j]);
    }
}

void iamr2D_97(double* m, int p, int niveau){
    int i;
    int arret;

    if(log2f((float) p) < niveau || niveau < 1)
        niveau = log2f((float) p);

    arret = pow(2, niveau-1);
    for(i=arret; i>0; i/=2){
        synthese2D_97(m, p, p, p/i, p/i);
    }
}